import org.uqbar.arena.Application

class DominoSPizza extends Application {
	
	new (PizzeriaBootstrap bootstrap) {
		super(bootstrap)
	}

	static def void main(String[] args) {
		new DominoSPizza(new PizzeriaBootstrap).start()
	}

	override protected createMainWindow() {
		new PedidosWindow(this)
	}
	
}
