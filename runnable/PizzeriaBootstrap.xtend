

import org.uqbar.arena.bootstrap.CollectionBasedBootstrap
import repo.RepoPedido
import org.uqbar.commons.applicationContext.ApplicationContext
import domain.Cliente
import repo.RepoCliente
import domain.Pedido
import domain.Ingrediente
import repo.RepoIngrediente
import domain.Pizza
import repo.RepoPizza
import domain.Plato

class PizzeriaBootstrap extends CollectionBasedBootstrap {

	new() {
		ApplicationContext.instance.configureSingleton(typeof(Cliente), new RepoCliente)
		ApplicationContext.instance.configureSingleton(typeof(Pedido), new RepoPedido)
		ApplicationContext.instance.configureSingleton(typeof(Ingrediente), new RepoIngrediente)
		ApplicationContext.instance.configureSingleton(typeof(Pizza), new RepoPizza)
	}
	
	override run() {
		val repoPedidos = ApplicationContext.instance.getSingleton(typeof(Pedido)) as RepoPedido
		val repoClientes = ApplicationContext.instance.getSingleton(typeof(Cliente)) as RepoCliente
		val repoIngredientes = ApplicationContext.instance.getSingleton(typeof(Ingrediente)) as RepoIngrediente
		val repoPizzas = ApplicationContext.instance.getSingleton(typeof(Pizza)) as RepoPizza
		
		
		repoClientes => [
			create("Juan","juano", "pass", "juan@mail.com", "encasa")
			create("Laura","lau", "pass", "lau@mail.com", "encasa")
			create("Gabriel","gabo", "pass", "gabo@mail.com", "enotracasa")
		]		
		
		// Genero un pedido custom con diferentes valores que se van a agregar al repo
		val pedido = new Pedido(repoClientes.searchCliente("gabo@mail.com"))
		pedido.toggleDelivery
		pedido.cancelar
		pedido.setAclaraciones("Un Estring de prueba")
		val anotherPedido = new Pedido(repoClientes.searchCliente("juano"))
		anotherPedido.toggleDelivery
		
		repoPedidos => [
			create(anotherPedido)
			create(new Pedido(repoClientes.searchCliente("lau")))
			create(pedido)
		]
		
		val salsa = new Ingrediente("Salsa", 10.00)
		val queso = new Ingrediente ("Queso", 15.00)
		val cebolla = new Ingrediente("Cebolla", 20.00)
		val morron = new Ingrediente("Morron", 30.00)
		val oregano = new Ingrediente("Oregano", 20.00)
		val aceituna = new Ingrediente("Aceituna", 5.00)
		
		repoIngredientes => [
			create(salsa)
			create(queso)
			create(cebolla)
			create(morron)
			create(oregano)
			create(aceituna)
		]
			
		repoPizzas => [
			create("Muzzarella", 25.00,#[salsa, queso])
			create("Cancha", 20.00, #[salsa])
			create("Fugazza", 15.00, #[cebolla, oregano])
		]
		
		val pl1 = new Plato
		pl1.pizza = new Pizza("Fuzzarella", 30.00, #[])
		anotherPedido.agregar(pl1)
	}
	
}
