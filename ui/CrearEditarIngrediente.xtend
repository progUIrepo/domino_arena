import domain.Ingrediente
import org.uqbar.arena.windows.WindowOwner
import org.uqbar.arena.aop.windows.TransactionalDialog
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.layout.ColumnLayout
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.widgets.Button

class CrearEditarIngrediente extends TransactionalDialog<Ingrediente>{
    
	new(WindowOwner window, Ingrediente ingrediente) {
		super(window, ingrediente)
		title = "Ingrediente"
	}
	
	override protected createFormPanel(Panel mainPanel) {	
		this.createIngredienteLayout(mainPanel)
		this.createBotoneraFooter(mainPanel)
	}
	
	def createBotoneraFooter(Panel panel) {
    	
    	val form = new Panel(panel).layout = new HorizontalLayout
		
		new Button(form) => [
			caption = "Aceptar"
			onClick([|this.accept])
			setAsDefault
			disableOnError
		]
		new Button(form) => [
			caption = "Cancelar"
			onClick([|this.cancel])
		]	
	}
	
	def createIngredienteLayout(Panel panel) {
			
	    val form = new Panel(panel).layout = new ColumnLayout(2)

		new Label(form).text = "Nombre"
		new Label(form).text = modelObject.nombre
		
		new Label(form).text = "Precio ($)"
		new Label(form).text = modelObject.precio.toString
	}
	
}	