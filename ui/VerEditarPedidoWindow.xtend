import domain.Pedido
import org.uqbar.arena.aop.windows.TransactionalDialog
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.WindowOwner
import org.uqbar.arena.layout.ColumnLayout
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.Button
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.layout.VerticalLayout
import org.uqbar.arena.widgets.TextBox
import static extension org.uqbar.arena.xtend.ArenaXtendExtensions.*
import org.uqbar.arena.widgets.tables.Table
import domain.Plato
import org.uqbar.arena.widgets.tables.Column
import org.uqbar.arena.windows.Dialog
import org.uqbar.arena.bindings.ObservableProperty
import appModel.EdicionPlatoAppModel
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.model.annotations.Observable
import org.uqbar.commons.model.annotations.Dependencies

@Accessors
@Observable
class VerEditarPedidoWindow extends TransactionalDialog<Pedido>{
    
   Plato platoSeleccionado
    
	new(WindowOwner window, Pedido pedido) {
		super(window, pedido)
		title = "Pedido "+ modelObject.id
	}
	
	override protected createFormPanel(Panel mainPanel) {	
		this.createEstadoHeader(mainPanel)
		new Label(mainPanel) => [
			text = "Platos"
			alignLeft
		]
		this.createABMPlatos(mainPanel)
		this.createAclaraciones(mainPanel)
		this.createDataCliente(mainPanel)
		this.createBotoneraFooter(mainPanel)
	}
	
	def createEstadoHeader(Panel panel) {
		val form = new Panel(panel).layout = new ColumnLayout(2)
		new Label(form).text = "Estado"
	}

	def createABMPlatos(Panel panel) {
	    val newform = new Panel(panel).layout = new HorizontalLayout
	    this.tablaDePlatos(newform)
		this.botoneraDeABM(newform).alignRight
	}
	
	def tablaDePlatos(Panel panel) {
		val gridPlatos = new Table(panel, typeof(Plato)) => [
			width = 700
			numberVisibleRows = 4
			items <=> "platos"
			bindValue(new ObservableProperty(this, "platoSeleccionado"))
		]
		new Column<Plato>(gridPlatos) => [
			title = "Nombre"
			bindContentsToProperty("pizza.nombre")
		]
		new Column<Plato>(gridPlatos) => [
			title = "Tamanio"
			bindContentsToProperty("tamanio").transformer = [tam | tam.toString]
		]
		new Column<Plato>(gridPlatos) => [
			title = "Precio"
			bindContentsToProperty("precio")
		]
	}

	def botoneraDeABM(Panel panel) {
		val form = new Panel(panel).layout = new VerticalLayout
		new Button(form) => [
			caption = "Agregar"
			onClick([|this.agregarPlato])
			bindEnabledToProperty("esAbierto")
		]
		new Button(form) => [
			caption = "Editar"
			onClick([|this.editarPlato])
			bindEnabledToProperty("esAbierto")
		]new Button(form) => [
			caption = "Eliminar"
			onClick([|eliminarPlatoSeleccionado])
			bindEnabledToProperty("esAbierto")
		]
	}
	

	def createAclaraciones(Panel panel) {
	    val form = new Panel(panel).layout = new VerticalLayout()
		new Label(form) => [
			text = "Aclaraciones"
			alignLeft
		]
		new TextBox(form) => [
		    value <=> "aclaraciones"
		    width = 300
   			height = 100
   			setMultiLine(true)
			bindEnabledToProperty("esAbierto")
		]
	}
	
	def createDataCliente(Panel panel) {

	    val form = new Panel(panel).layout = new ColumnLayout(2)

		new Label(form).text = "Cliente"
		new Label(form).text = modelObject.cliente.nombre.toUpperCase
		
		new Label(form).text = "Costo de envío"
		new Label(form).text = "$ " + modelObject.costoDelivery.toString
		
		new Label(form).text = "Monto Total"
		
		new Label(form) => [
			value <=> "montoTotal"
		]//.text = "$ " + modelObject.montoTotal.toString

		new Label(form).text = "Fecha"
		new Label(form).text = modelObject.fecha.toString
	}
	
	def createBotoneraFooter(Panel panel) {
	    val form = new Panel(panel).layout = new HorizontalLayout
		new Button(form) => [
			caption = "Aceptar"
			onClick([|])
		]
		new Button(form) => [
			caption = "Cancelar"
			onClick([|this.close])
		]
	}
	
	def eliminarPlatoSeleccionado(){
		modelObject.eliminar(platoSeleccionado)
	}

	def editarPlato() {
		this.openDialog(new AgregarEditarPlatoWindow(this, new EdicionPlatoAppModel(platoSeleccionado)))
	}
	
	def agregarPlato() {
		this.openDialog(new AgregarEditarPlatoWindow(this, new EdicionPlatoAppModel(new Plato)))
	}
	
	def openDialog(Dialog<?> dialog) {
		dialog.onAccept[|modelObject.agregar(platoSeleccionado)]
		dialog.open
	}
}
