import appModel.PedidosAppModel
import appModel.PedidosCerradosAppModel
import domain.Pedido
import org.uqbar.arena.bindings.NotNullObservable
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.layout.VerticalLayout
import org.uqbar.arena.widgets.Button
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.widgets.tables.Column
import org.uqbar.arena.widgets.tables.Table
import org.uqbar.arena.windows.Dialog
import org.uqbar.arena.windows.SimpleWindow
import org.uqbar.arena.windows.WindowOwner

import static extension org.uqbar.arena.xtend.ArenaXtendExtensions.*
import java.time.LocalDateTime
import org.uqbar.arena.windows.ErrorsPanel
import org.uqbar.commons.model.exceptions.UserException
//import domain.NoSePuedeCambiarDeEstadoException

class PedidosWindow extends SimpleWindow<PedidosAppModel>{
	
	new(WindowOwner parent) {
		super(parent, new PedidosAppModel)
		modelObject.abiertos
	}

	override def createMainTemplate(Panel mainPanel) {
		title = "Dominos Pizza"
		
		taskDescription = "Pedidos abiertos"

		super.createMainTemplate(mainPanel)

		this.createPanelSuperior(mainPanel)
		this.createPanelInferior(mainPanel)

	}
	def createPanelSuperior(Panel panel) {
		val superior = new Panel(panel).layout = new HorizontalLayout
		this.createResultsGrid(superior)
		this.createGridActions(superior)
	}

	def createPanelInferior(Panel panel) {
		val inferior = new Panel(panel).layout = new HorizontalLayout
        
		new Button(inferior) => [
			caption = "Menu"
			onClick([|])
		]
		new Button(inferior) => [
			caption = "Pedidos cerrados"
			onClick([|this.verCerrados])
		]
		new Button(inferior) => [
			caption = "Salir"
			onClick([|this.close])
		]
	}
	
	def verCerrados() {
		new PedidosCerradosWindow(this, new PedidosCerradosAppModel).open
	}

	def createGridActions(Panel panel) {
		
		val elementSelected = new NotNullObservable("pedidoSeleccionado")

		val actionsPanel = new Panel(panel).layout = new VerticalLayout		
		estadosPanel(actionsPanel, elementSelected)
		
		new Button(actionsPanel) => [
			caption = "Cancelar"
			onClick([|modelObject.cancelarPedido; modelObject.abiertos])
			bindEnabled(elementSelected)
		]
		
		new Button(actionsPanel) => [
			caption = "Editar"
			onClick([|this.modificarPedido])
			bindEnabled(elementSelected)
		]
	}

	def estadosPanel(Panel panel, NotNullObservable selected) {
		
		val estados =new Panel(panel).layout = new HorizontalLayout
		
		new Button(estados) => [
			caption = "<<<"
			onClick([|this.estadoPrevio; modelObject.abiertos])
			bindEnabled(selected)
			disableOnError
		]
		new Button(estados) => [
			caption = ">>>"
			onClick([|this.siguienteEstado(modelObject)])
			bindEnabled(selected)
			disableOnError
		]
	}
	
	def siguienteEstado(PedidosAppModel model) {
		try {
			model.pedidoSeleccionado.siguienteEstado
		} catch (UserException no)
	
		modelObject.abiertos
	}
	
	def estadoPrevio() {
		modelObject.pedidoSeleccionado.anteriorEstado
	}

	def modificarPedido() {
		this.openDialog(new VerEditarPedidoWindow(this, modelObject.pedidoSeleccionado))
	}
	
	def createResultsGrid(Panel mainPanel) {
		val table = new Table<Pedido>(mainPanel, typeof(Pedido)) => [
			items <=> "pedidos"
			value <=> "pedidoSeleccionado"
			numberVisibleRows = 5
		]
		this.describeResultsGrid(table)
	}
	
	def describeResultsGrid(Table<Pedido> table) {
		
		new Column<Pedido>(table) => [
			title = "Pedido"
			fixedSize = 100
			bindContentsToProperty("id").transformer = [Integer i | "Pedido "+ i]
		]
		new Column<Pedido>(table) => [
			title = "Estado"
			fixedSize = 100
			bindContentsToProperty("estado")
		]
		new Column<Pedido>(table) => [
			title = "Monto"
			fixedSize = 100
			bindContentsToProperty("precio").transformer = [double d | "$ " + d]
		]
		new Column<Pedido>(table) => [
			title = "Hora"
			fixedSize = 100
			bindContentsToProperty("fecha").transformer = [LocalDateTime ld | ld.toLocalTime.toString]
		]
	}
	
	override protected addActions(Panel arg0) {
	}
	
	override protected createFormPanel(Panel pan) {
	}
	
	def openDialog(Dialog<?> dialog) {
		dialog.onAccept[|modelObject.abiertos]
		dialog.open
	}
}