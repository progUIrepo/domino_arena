

import org.uqbar.arena.aop.windows.TransactionalDialog
import org.uqbar.arena.windows.WindowOwner
import org.uqbar.arena.widgets.Panel
import appModel.EdicionPlatoAppModel
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.Selector

import static extension org.uqbar.arena.xtend.ArenaXtendExtensions.*
import domain.Pizza
import org.uqbar.arena.layout.VerticalLayout
import domain.Ingrediente
import org.uqbar.arena.layout.ColumnLayout
import org.uqbar.arena.widgets.CheckBox
import org.uqbar.arena.widgets.RadioSelector
import org.uqbar.lacar.ui.impl.jface.radiogroup.RadioGroup
import org.uqbar.arena.widgets.Button

class AgregarEditarPlatoWindow extends TransactionalDialog<EdicionPlatoAppModel> {
	
	new(WindowOwner owner, EdicionPlatoAppModel model) {
		super(owner, model)
		modelObject.promos
		modelObject.ingredientes
		title = "Plato"
	}
	
	override protected createFormPanel(Panel pan) {
		
		this.promoSelector(pan)
		this.tamanioSelector(pan)
		this.seccionAregados(pan)
		this.createBotoneraFooter(pan)
	}
	
	def seccionAregados(Panel panel) {
		val agre = new Panel(panel).layout = new VerticalLayout
		new Label(agre).text = "Agregados"
		
		this.seccionIngredientes(agre)
	}
	
	def seccionIngredientes(Panel panel) {
		val ingPan = new Panel(panel).layout = new VerticalLayout
		for (in : modelObject.ingredientes) {
			lineaConfig(in, ingPan)
		}	
	}
	
	def Panel lineaConfig(Ingrediente ingrediente, Panel panel) {
		val linea = new Panel(panel).layout = new HorizontalLayout
		new CheckBox(linea) => [
		] 
		new Label(linea).text = ingrediente.toString
		new RadioSelector(linea) => [	
		]
		
		linea
	}
	
	def tamanioSelector(Panel panel) {
		val tam = new Panel(panel).layout = new HorizontalLayout
		new Label(tam).text = "Tamanio"
	}
	
	def promoSelector(Panel panel) {
		val prom = new Panel(panel).layout = new HorizontalLayout
		new Label(prom).text = "Pizza"
		
		new Selector<Pizza>(prom) => [
			allowNull(false)
			(items <=> "pizzas").adaptWith(typeof(Pizza), "nombre")
			
			//bindItemsToProperty("pizzas").transformer = [ p |p.toString]
			//(items <=> "pizzas")
			//val propiedadPizzas = bindItems(new ObservableProperty(repoModelos, "modelos"))
			//propiedadPizzas.adaptWith(typeof(Pizza), "descripcionEntera") // opción A
			//propiedadModelos.adapter = new PropertyAdapter(typeof(Modelo), "descripcionEntera") // opción B
		]
	}
	def createBotoneraFooter(Panel panel) {
	    val form = new Panel(panel).layout = new HorizontalLayout
		new Button(form) => [
			caption = "Aceptar"
			onClick([|])
		]
		new Button(form) => [
			caption = "Cancelar"
			onClick([|this.close])
		]
	}
	
}