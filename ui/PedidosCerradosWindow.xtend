

import org.uqbar.arena.windows.SimpleWindow
import org.uqbar.arena.windows.WindowOwner
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.widgets.tables.Table


import static extension org.uqbar.arena.xtend.ArenaXtendExtensions.*
import domain.Pedido
import org.uqbar.arena.widgets.tables.Column
import appModel.PedidosCerradosAppModel
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.widgets.Button
import org.uqbar.arena.bindings.NotNullObservable
import org.uqbar.arena.windows.Dialog
import java.time.LocalDateTime

class PedidosCerradosWindow extends SimpleWindow<PedidosCerradosAppModel> {
	
	new(WindowOwner parent, PedidosCerradosAppModel model) {
		super(parent, model)
		modelObject.cerrados
	}

	override def createMainTemplate(Panel mainPanel) {
		title = "Dominos Pizza"
		taskDescription = "Pedidos cerrados"

		super.createMainTemplate(mainPanel)
		
		this.createResultsGrid(mainPanel)
		this.createPanelInferior(mainPanel)
	}

	def createResultsGrid(Panel mainPanel) {
		val table = new Table<Pedido>(mainPanel, typeof(Pedido)) => [
			items <=> "pedidosCerrados"
			value <=> "pedidoSeleccionado"
			numberVisibleRows = 5
		]
		this.describeResultsGrid(table)
	}
	
	def describeResultsGrid(Table<Pedido> table) {
		
		new Column<Pedido>(table) => [
			title = "Pedido"
			fixedSize = 100
			bindContentsToProperty("id").transformer = [Integer i | "Pedido "+ i]
		]
		new Column<Pedido>(table) => [
			title = "Estado"
			fixedSize = 100
			bindContentsToProperty("estado")
		]
		new Column<Pedido>(table) => [
			title = "Fecha"
			fixedSize = 100
			bindContentsToProperty("fecha").transformer = [LocalDateTime ld | ld.toLocalDate.toString]
		]
		new Column<Pedido>(table) => [
			title = "Tiempo de espera"
			fixedSize = 100
			bindContentsToProperty("tiempoDeEspera")
		]
	}

	def createPanelInferior(Panel panel) {
		val inferior = new Panel(panel).layout = new HorizontalLayout
        val elementSelected = new NotNullObservable("pedidoSeleccionado") 
		new Button(inferior) => [
			caption = "Ver"
			onClick([|this.modificarPedido])
			bindEnabled(elementSelected)
		]
		new Button(inferior) => [
			caption = "Volver"
			onClick([|this.close])
		]
	}
	
	def modificarPedido() {
		this.openDialog(new VerEditarPedidoWindow(this, modelObject.pedidoSeleccionado))
    }	
    
	override protected addActions(Panel arg0) {
	}

	override protected createFormPanel(Panel arg0) {
	}
	
	def openDialog(Dialog<?> dialog) {
		//dialog.onAccept[|]
		dialog.onCancel[|]
		dialog.open
	}	
}